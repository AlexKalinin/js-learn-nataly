var STORAGE_KEY = 'iss3_localstorage_ajax';

/**
 * сохранить obj в localStorage
 * @param obj
 */
function saveToLocalStorage(obj){
    localStorage.setItem(STORAGE_KEY, JSON.stringify(obj));
}

/**
 * получить объект из localStorage
 * @returns {*}
 */
function loadFromLocalStorage(){
    return JSON.parse(localStorage.getItem(STORAGE_KEY));
}


//todo: покрыть тестами, написать комментарий-документацию
function getAttr(inpt, attrName){
    //тут можно поставить проверки
    //например:
    // если inpt не является HTMLInputElement то у него же не будет вообще атрибутов!
    if(!inpt){
        return undefined;
    }
    // если inpt не содержит такого аттрибута как attrName, то обращение к методу .value у типа undefined вызовет ошибку!
    if(!inpt.attributes[attrName]){ //потому что выражение `(undefined) ? true : false` возвращает false
        return undefined;
    }
    return inpt.attributes[attrName].value;
}

function buildObject(){
    var  arr = [];
    var inputs = document.body.getElementsByTagName("input");
    for(var i = 0; i < inputs.length; i++){
        var inputType = getAttr(inputs[i], 'data-type');
        if(inputType !== undefined && inputType === 'cell'){

            //todo: build 2-dimention array here from inputs
            var r = getAttr(inputs[i], 'data-r');
            r = parseInt(r); //индексы не могут быть строками, им нужно быть числами! (getAttr всегда возвращает либо строку либо undefined)
            var c = getAttr(inputs[i], 'data-c');
            c = parseInt(c); //индексы не могут быть строками, им нужно быть числами! (getAttr всегда возвращает либо строку либо undefined)
            var val = inputs[i].value;

            if(typeof arr[r] === 'undefined') {
                arr[r] = [];
            }
            arr[r][c] = val;
        }
    }
    return arr;
}

//todo: покрыть тестами, написать комментарий-документацию
function generateEmptyObject(rows, columns){
    var myObj = [];

    for(var i = 0; i < rows; i++){
        myObj[i] = [];
        for(var j = 0; j < columns; j++){
            myObj[i][j] = "";
        }
    }
    return myObj;
}

/**
 * Generate HTML table
 */
function generateEmptyHtmlTable(rows, columns) {
    var obj = generateEmptyObject(rows, columns);
    return generateHtmlTableFromObject(obj);
}

//todo: покрыть тестами, написать комментарий-документацию
function generateHtmlTableFromObject(obj){
    var table = document.createElement("table"),
        checkbox =  document.getElementById("checkbox");
    table.setAttribute("border", (checkbox.checked ? "1px" : "0")); //тернарный оператор
    table.setAttribute("width",   document.getElementById("table-width").value);
    table.setAttribute("height",  document.getElementById("table-height").value);

    for (var i = 0; i < obj.length; i++){
        var tr = document.createElement("tr");
        for (var j = 0; j < obj[i].length; j++){
            var td = document.createElement("td");
            //todo: выделить td и input в виде html-элементов
            //todo: проследить, чтобы значение отображалось в input корректно (сейчас теряются спецсимволы)
            tr.innerHTML = tr.innerHTML + "<td>" + "idx[" + i + ", " + j + '] <input type="text" data-type="cell" data-r="' + i + '" data-c="' + j + '" value="' + obj[i][j] + '">' + "</td>";
        }
        table.innerHTML = table.innerHTML + tr.outerHTML;
    }
    return table.outerHTML;
}

/**
* Задаент внутренний html div-нику tableHolder
*
*/
function setTableHolder(html){
    var div = document.getElementById("tableHolder");
    div.innerHTML = html;
}







// ------- Обработка нажатий на кнопки --------

//todo: написать комментарий-документацию
function btnClick_saveToLocalStorage() {
    // debugger;
    var obj = buildObject();
    saveToLocalStorage(obj);

}

//todo: написать комментарий-документацию
function btnClick_loadFromLocalStorage(){
    var obj = loadFromLocalStorage();
    var htmlTable = generateHtmlTableFromObject(obj);
    setTableHolder(htmlTable);

}

//todo: написать комментарий-документацию
function btnClick_generateEmptyHtmlTable(){
    var rows = document.getElementById("rows").value;
    var columns = document.getElementById("columns").value;

    var htmlTable = generateEmptyHtmlTable(rows, columns);
    
    setTableHolder(htmlTable);
}

//todo: написать комментарий-документацию
function btnClick_destroyHtmlTable(){
    setTableHolder('');
}