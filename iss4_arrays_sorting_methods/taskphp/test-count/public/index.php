<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Проверка работы count</title>
  </head>
  
<body>
<h1>Проверка работы count</h1>

<pre>

<?php

require "../TestCountArray.class.php";

$t = new TestCountArray();
$t->buildArray(10000);

//подсчет суммы плохим способом
$time = microtime(false);
$badSum = $t->calcSumBad();
$badTime = microtime(false) - $time;


//подсчет суммы хорошим способом
$time = microtime(false);
$goodSum = $t->calcSumGood();
$goodTime = microtime(false) - $time;


//вывод результатов
echo "calcSumBad(): result: $badSum; time: $badTime; \n";
echo "calcSumGood(): result: $goodSum; time: $goodTime; \n";

?>
</pre>

</body>
</html>


