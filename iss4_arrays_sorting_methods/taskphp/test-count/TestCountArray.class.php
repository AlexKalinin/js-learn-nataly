<?php

class TestCountArray{

	public $arr;
	
	/**
	 * Построение заполненного массива заданного размера $size
	 */
	public function buildArray($size){ //метод
		for($i = 0; $i < $size; $i++){
			$this->arr[$i] = rand(1, 1000);
		}
	}
	
	/**
	 * Вычисление суммы массива без допушения ошибки
	 */
	public function calcSumGood(){
		$i = 0;
		$l = count($this->arr); //count вычисляется 1 раз!
		$sum = 0;
		while($i < $l){
			$sum += $this->arr[$i];
			$i++;
		}
		return $sum;
	}
	
	/**
	 * Вычисление суммы массива С допушением ошибки
	 */
	public function calcSumBad(){
		$i = 0;
		$sum = 0;
		while($i < count($this->arr)){ //count вычисляется каждый раз!
			$sum += $this->arr[$i];
			$i++;
		}
		return $sum;
	}
}
