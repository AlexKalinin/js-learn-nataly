<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Тикет 4, задача 005 (портирование JS на php, буквы внутри строки)</title>

</head>
<body>
<h1>Тикет 4, задача 005 (портирование JS на php, буквы внутри строки)</h1>

<p>Итак, начинаем!</p>
    <pre>
      <?php
      //весь код запуска будет у нас тут,
      //т.к. нам ненужен контроль над остальной частью страницы
      // в современном PHP необвернутый в классы код используется
      // как точка входа (запуска) всего приложения
      // такого кода очень мало.


      //мы хотим видеть все ошибки в браузере!
      // обычно логи лежат тут http://stackoverflow.com/questions/3719549/where-does-phps-error-log-reside-in-xampp
      error_reporting(E_ALL);
      ini_set('display_errors', 1);

      //подключаем наш класс по пути ../Task005.php
      define('__ROOT__', dirname(dirname(__FILE__)));
      require_once(__ROOT__ . '/Task005.php');

      //теперь код класса Task005 загружен из файла и мы можем спокойно начать его использовать

      //создаем новый экземпляр (объект) класса Task005
      // для аналогии:
      // $t005 - это яблоко которое держу в руке
      // class Task005{ ... } -- это классификация фрукта "яблоко"
      $task005 = new Task005();
      $task005->letterExists($arr);
      $v1 = letterExists($arr);
      $v2 = false;
      $task005->test($v1, $v2, $comment);
      $comment = "it should return false on empty object";


      ?>
    </pre>
</body>
</html>

