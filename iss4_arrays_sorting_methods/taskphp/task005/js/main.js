/**
 *   See console log output for details.
 *   @author: Alex Kalinn
 *   @email: login.hedin@gmail.com
 */


/**
 * Self-written jasmine's expect() test function analog
 */
function expect(val, comment) {
    this.toEqual = function (val2) {
        var isSuccess = true;
        var mOk = "TEST: OK...";
        var mErr = "TEST: FAILED: " + comment + ". |==> Expected " + JSON.stringify(val2) + ", but got " + JSON.stringify(val);
        if (val !== val2 || JSON.stringify(val) !== JSON.stringify(val2)) {
            console.log(mErr);
            return;
        }
        console.log(mOk);
    };
    return this;
}

// Написать функцию, куда будет передан в виде параметра функции — массив из двух элементов.
// Задача функции — вернуть значение true, если строка в первом элементе массива содержит все буквы строки из второго элемента массива.
// Function exampleFunction ([“HeLlo”, “LeHo”]) // true
// Ваша функция будет протестирована на различных вариациях пар слов.
function letterExists(arr) {
    if (typeof(arr) !== "object" || arr.length === undefined || arr.length !== 2) {
        return false;
    }

    var srcArr = arr[0].split("");
    var ptrnArr = arr[1].split("");

    for (var i = 0; i < ptrnArr.length; i++) {
        var ltMatch = false;
        for (var j = 0; j < srcArr.length; j++) {
            if (ptrnArr[i].toLowerCase() === srcArr[j].toLowerCase()) {
                ltMatch = true;
            }
        }
        if (!ltMatch) {
            return false;
        }
    }
    //
    return true;
}


//invalid arguments
expect(letterExists(['HeLlo']), "it should return false on invalid argument").toEqual(false);
expect(letterExists(['HeLlo', 'He', 'HeLlo ignoring...']), "it should return false on 3d element").toEqual(false);
expect(letterExists([]), "it should return false on on empty array").toEqual(false);
expect(letterExists(false), "it should return false on false").toEqual(false);
expect(letterExists(undefined), "it should return false on undefined").toEqual(false);
expect(letterExists({}), "it should return false on empty object").toEqual(false);

//contains
expect(letterExists(['HeLlo', 'LeHo']), "it should return true on LeHo").toEqual(true);
expect(letterExists(['world', 'rwD']), "it should return true on rwD").toEqual(true);
expect(letterExists(['HeLlo', 'Le2']), "it should return false on Le2").toEqual(false);
expect(letterExists(['Other string', 'LeHo']), "it should return false on invalid argument").toEqual(false);
expect(letterExists(['Съешь ещё этих мягких французских булок, да выпей же чаю', 'сЪёфбдь ю']), "it should return true on long strings").toEqual(true);


