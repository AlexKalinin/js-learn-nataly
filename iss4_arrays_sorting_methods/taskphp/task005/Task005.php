<?php

class Task005
{

    /**
     * __construct() - это специальный метод, называется конструктором
     *
     */
    public function __construct()
    {

    }

    /**
     * Self-written jasmine's expect() test function analog
     */
    public function test($v1, $v2, $comment)
    {
        $mOk = "TEST: OK...";
        $mErr = "TEST: FAILED: " . $comment . ". |==> Expected " . json_encode($v2) . ", but got " . json_encode($v1);
        if ($v1 !== $v2 || json_encode($v1) !== json_encode($v2)) {
            echo $mErr;
            return;
        }
        echo $mOk;
        return;
    }

    public function letterExists($arr)
    {
        $length = count($arr);
        if (gettype($arr) !== "object" || $length === null || $length !== 2) {
            return false;
        }

        $srcArr = explode(" ", $arr[0]);
        $ptrnArr = explode("", $arr[1]);

        for ($i = 0; $i < count($ptrnArr); $i++) {
            $ltMatch = false;
            for ($j = 0; $j < count($srcArr); $j++) {
                if (strtolower($ptrnArr[$i]) === strtolower($srcArr[$j])) {
                    $ltMatch = true;
                }
            }
            if (!$ltMatch) {
                return false;
            }
        }
        return true;
    }

}