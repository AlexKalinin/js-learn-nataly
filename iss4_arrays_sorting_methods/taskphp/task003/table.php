<?php

    $rows = 20;
    $cols = 20;

    $table = '<table border="1">';

    for ($tr=1; $tr<=$rows; $tr++){
        $table .= '<tr>';
        for ($td=1; $td<=$cols; $td++){
            if ($tr===1 or $td===1){
                $table .= '<th style="color:white;background-color:green;">'. $tr*$td .'</th>';
            }else{
                $table .= '<td><strong>'. $tr*$td .'</strong></td>';
            }
        }
        $table .= '</tr>';
    }

    $table .= '</table>';
    echo $table;

?>