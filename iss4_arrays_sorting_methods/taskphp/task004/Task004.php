<?php

//класс - это контейнер, в который обворачивается код
//В php классы называются с большой буквы, в стиле CamelCase,
// 1 файл должен быть 1 класс
// начало тега <?php должно присутствовать
// конец тега ?_> отсутствует.
class Task004
{

    /**
     * __construct() - это специальный метод, называется конструктором
     * Когда мы выполняем код:
     * $t004 = new Task004();
     * это означает, что сначала
     * инициализируются все поля класса Task004 (переменные $BEGIN_MILLISECONDS и $msg_number)
     * затем запускается функция __construct() у класса Task004 -- обычно сюда пишут подготовительную работу
     * которую нужно провести над полями класса (переменными)
     */
    public function __construct()
    {
        $this->BEGIN_MILLISECONDS = microtime();
    }


    // public -- модификатор, означает, мы можем получить доступ ИЗВНЕ класса
    // 0 - потому что в PHP я не могу переменной сразу присвоить значение из функции, 
    //     только простейшее, которое не нужно вычислять
    // настоящее значение попадет в конструкторе __construct()
    public $BEGIN_MILLISECONDS = 0;

    //вне класса эту переменную мне не нужно использовать.
    private $msg_number = 0;

    /**
     * Логгирует $msg сообщение
     */
    public function info($msg)
    {
        // к НЕСТАТИЧНЫМ методам класса мы обращаемся через $this. 
        // $this это как this в javascript -- означает текущий экземпляр класса
        $this->msg_number++;
        $diff = microtime() - $this->BEGIN_MILLISECONDS;
        $msg = "[line " . $this->msg_number . "]\t[INFO]\t[" . $diff . " мс]\t" . $msg . "\n";
        echo $msg;

        $fp = fopen(__ROOT__ . '/log/application.log', 'a');
        fwrite($fp, $msg);
        fclose($fp);
    }

    /**
     * Превращает объект в строку
     * @param $obj
     * @return string
     */

    public function toS($obj)
    {
        return json_encode($obj);
    }

    /**
     * Сформировать массив размером
     * @param $size
     * @return array
     */
    public function buildArray($size)
    {
        $this->info("buildArray(): Зашли в функцию buildArray(), size = " . $size);

        $this->info("buildArray(): Выделяем память под массив...");
        $arr = array($size);
        $this->info("buildArray(): Получили переменную arr: " . $this->toS($arr) . " с длинной: " . count($arr));

        $this->info("buildArray(): Заполняем arr случайными данными");
        for ($idx = 0; $idx < count($arr); $idx++) {
            $this->info("buildArray(): Вошли внутрь цикла for, idx = " . $idx);
            $rnd = (rand() * 10);
            $arr[$idx] = $rnd;
            $this->info("buildArray(): Заполнили элемент массива значением: " . $arr[$idx]);
        }
        $this->info("buildArray(): Закончили заполнять массив, теперь он равен: " . $this->toS($arr));
        return $arr;
    }

    /**
     * Распечатывает массив на экран
     * @param $arr
     */
    public function printArray($arr)
    {
        $this->info("printArray(): Зашли в функцию printArray(), полученный arr = " . $this->toS($arr));
        $i = 0;
        $arrString = ""; //будем выводить в строку
        $length = count($arr);
        while ($i < $length) {
            $arrString .= "arr[$i]=$arr[$i]; ";
            $i++;
        }
        $this->info("printArray(): " . $arrString);
    }

    /**
     * сортировка пузырьком
     * @param $a
     * @return mixed
     */
    public function sortArrayBubbles($a)
    {
        $this->info("sortArrayBubbles(): зашли в функцию сортировкой пузырьком, массив a = " . $this->toS($a));
        $t = true;//можно ли еще сортировать
        while ($t) {
            $t = false;//для начала, считаем, что нельзя
            for ($i = 0; $i < count($a) - 1; $i++) {// пробегаем по всему массиву, кроме последнего элемента
                if ($a[$i] > $a[$i + 1]) {// если элемент, на который мы смотрим больше следующего /*1*/
                    $temp = $a[$i + 1];//это три действия для обмена местами элементов/*2*/
                    $a[$i + 1] = $a[$i];//---//---/*3*/
                    $a[$i] = $temp;//пузырек всплывает на одну позицию вверх.
                    $t = true;//раз что-то изменили, есть шанс, что сортировка еще не окончена
                }
            }
        }
        $this->info("sortArrayBubbles(): закончили сортировать массив, возвращаем a, который теперь равен " . $this->toS($a));
        return $a;
    }


    /**
     * Функция для тестирования
     * @param $comment
     * @param $value1
     * @param $value2
     */
    public
    function test($comment, $value1, $value2)
    {
        $testMessage = "ТЕСТ ПРОЙДЕН!";
        if ($this->toS($value1) !== $this->toS($value2)) {
            $testMessage = "ТЕСТ ПРОВАЛЕН: Ожидалось значение: " . $this->toS($value1) . ", а получили: " . $this->toS($value2);
        }
        $this->info("test(): " . $comment . " ..." . $testMessage);
    }
}