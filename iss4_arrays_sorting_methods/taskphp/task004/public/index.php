<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Тикет 4, задача 004 (портирование JS на php, сортировка)</title>

</head>
<body>
<h1>Тикет 4, задача 004 (портирование JS на php, сортировка)</h1>

<p>Итак, начинаем!</p>
    <pre>
      <?php

      //весь код запуска будет у нас тут,
      //т.к. нам ненужен контроль над остальной частью страницы
      // в современном PHP необвернутый в классы код используется
      // как точка входа (запуска) всего приложения
      // такого кода очень мало.


      //мы хотим видеть все ошибки в браузере!
      // обычно логи лежат тут http://stackoverflow.com/questions/3719549/where-does-phps-error-log-reside-in-xampp
      error_reporting(E_ALL);
      ini_set('display_errors', 1);

      //подключаем наш класс по пути ../Task004.php
      define('__ROOT__', dirname(dirname(__FILE__)));
      require_once(__ROOT__ . '/Task004.php');

      //теперь код класса Task004 загружен из файла и мы можем спокойно начать его использовать

      //создаем новый экземпляр (объект) класса Task004
      // для аналогии:
      // $t004 - это яблоко которое держу в руке
      // class Task004{ ... } -- это классификация фрукта "яблоко"
      $t004 = new Task004();

      //поскольку я уже за пределами класса Task004, доступ к переменной $BEGIN_MILLISECONDS возможен только через экземпля класса $t004->BEGIN_MILLISECONDS
      //при условии что она публичная
      $t004->info("MAIN: Старт программы. Здравствуйте! Колличество миллисекунд от 01.01.1970: " . $t004->BEGIN_MILLISECONDS);

      $t004->info("MAIN: Построим массив mayArr...");
      $myArr = $t004->buildArray(10);

      $t004->info("MAIN: Распечатаем массив mayArr...");
      $t004->printArray($myArr);

      $t004->info("MAIN: Сортируем массив...");
      $t004->sortArrayBubbles($myArr);

      $t004->info("MAIN: Снова распечатаем массив mayArr...");
      $t004->printArray($myArr);

      $t004->info("MAIN: Теперь запустим тестирование!");

      $t004->test("тестируем сложение (проверка тестирования)", 12 + 12, 24);
      $t004->test("тестируем истину (проверка тестирования)", true, true);

      $t004->test("тестируем NULL (проверка тестирования)", NULL, NULL);
      $t004->test("Тестируем сортировку массива методом пузырька", $t004->sortArrayBubbles([10, 1, 9, 2, 8, 3, -1, 4]), [-1, 1, 2, 3, 4, 8, 9, 10]);

      $t004->info("MAIN: Программа завершена. До свидания! Колличество миллисекунд от 01.01.1970: " . microtime());
      ?>
    </pre>
</body>
</html>

