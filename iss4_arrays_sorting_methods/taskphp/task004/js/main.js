//запоминаем, сколько было миллисекунд от 01.01.1970 в момент старта скрипта
// это нужно, чтобы в сообщениях показывать, сколько по времени миллисекунд
// прошло после выполнения программы
// для PHP можешь взять http://php.net/manual/en/function.microtime.php
var BEGIN_MILLISECONDS = new Date().getTime();
var msg_number = 0;

// //ПРОПУСКАЕМ ПОКА ЧТО РЕАЛИЗАЦИЮ ЭТОЙ ФУНКЦИИ.
// //Подсветка html-тегами строки str. Возрващает сформатированную html-строку
// function colorizeString(str){
//   var rgx = [
//     {
//       regex: /(^\[line\s\d+\])/gmi,
//       style: "color: #7b7730",
//       comment: "Подсветка номера строки в логах"
//     }
//     ,{
//       regex: /(\[INFO\])/gmi,
//       style: "color: #39783a",
//       comment: "Подсветка уровеня лога - INFO"
//     }
//     ,{
//       regex: /(\[\d+ мс\])/gmi,
//       style: "color: #7f471d",
//       comment: "Подсветка времени выполнения"
//     }
//     ,{
//       regex: /(MAIN:)/gmi,
//       style: "color: #c52026",
//       comment: "Подсветка строк из главной программы"
//     }
//     ,{
//       regex: /(\w+\(\))/gmi,
//       style: "color: #2c2e80",
//       comment: "Подсветка любой функции"
//     }
//   ];


//   var concatStr = "";
//   for(var i = 0; i < rgx.length; i++){
//     var matchRes = str.match(rgx[i].regex);
//     if(matchRes !== null){
//       var arrStrings = str.split(rgx[i].regex);
//       for(var j = 0; j < arrStrings.length; j++){
//         if(arrStrings[j] === matchRes[0]){
//           arrStrings[j] = "<span style = '" + rgx[i].style + "'>" + arrStrings[j] + '</span>';
//         }
//       }
//       str = arrStrings.join("");
//     }
//   }
//   return str;
// } //end colorizeString()




//протоколируем (регистрируем) текстовые сообщения msg
function info(msg){
    msg_number++;
    //вычисляем разницу между моментом старта программы и вызовом данной функции
    var diff = new Date().getTime() - BEGIN_MILLISECONDS;
    //форматируем msg для вывода: [INFO][188381] какой-то текст из msg
    msg = "[line " + msg_number + "]\t[INFO]\t[" + diff + " мс]\t" + msg;

    //выводим в консоль:
    //для php надо использовать http://php.net/manual/en/function.echo.php
    //чтобы вывести внутри <pre id="monitor"></pre>
    console.log(msg);

    // //также выводим на экран
    // var mon =  document.getElementById("monitor");
    // // \n - означает перенести строку на следующую
    // mon.innerHTML = mon.innerHTML + "\n" + colorizeString(msg);
}

//превращает объект в строку
function toS(obj){
    //аналог на php: http://php.net/manual/en/function.json-encode.php
    return JSON.stringify(obj);
}

//сформировать массив размером
function buildArray(size){
    info("buildArray(): Зашли в функцию buildArray(), size = " + size);

    info("buildArray(): Выделяем память под массив...");
    var arr = new Array(size);
    info("buildArray(): Получили переменную arr: " + toS(arr) + " с длинной: " + arr.length);

    info("buildArray(): Заполняем arr случайными данными");
    for(var idx = 0; idx < arr.length; idx++){
        info("buildArray(): Вошли внутрь цикла for, idx = " + idx);
        //см. Случайные числа в php: http://php.net/manual/ru/function.rand.php
        //ну и сами Math.floor и Math.random надо прочитать в JavaScript
        //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/floor
        //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
        var rnd = Math.floor(Math.random() * 10);
        arr[idx] = rnd;
        info("buildArray(): Заполнили элемент массива значением: " + arr[idx]);
    }
    info("buildArray(): Закончили заполнять массив, теперь он равен: " + toS(arr));
    return arr;
}

//распечатывает массив на экран
function printArray(arr){
    info("printArray(): Зашли в функцию printArray(), полученный arr = " + toS(arr));
    var i = 0;
    var arrString = ""; //будем выводить в строку
    while(i < arr.length){
        arrString += "arr[" + i + "]=" + arr[i] + "; ";
        i++;
    }
    info("printArray(): " + arrString);
}

// сортировка пузырьком
function sortArrayBubbles(a){
    info("sortArrayBubbles(): зашли в функцию сортировкой пузырьком, массив a = " + toS(a));

    //здесь код сортировки пузырьком
    // ...

    //а я воспользуюсь sort :)
    a.sort(); //закомментируй и напиши сама!



    info("sortArrayBubbles(): закончили сортировать массив, возвращаем a, который теперь равен " + toS(a));
    return a;
}

//функция для тестирования (зачем нам целый Jasmine?!!)
function test(comment, value1, value2){
    var testMessage = "ТЕСТ ПРОЙДЕН!";
    //var testMessage = "<b style='color: green'>ТЕСТ ПРОЙДЕН!</b>";
    if(toS(value1) !== toS(value2)){
        testMessage = "ТЕСТ ПРОВАЛЕН: Ожидалось значение: " + toS(value1) + ", а получили: " + toS(value2);
    }
    info("test(): " + comment + " ..." + testMessage);
}


// !!!!!! НАЧИНАЕМ ВЫПОЛНЯТЬ ПРОГРАММУ  !!!!!!

info("MAIN: Старт программы. Здравствуйте! Колличество миллисекунд от 01.01.1970: " + BEGIN_MILLISECONDS);

info("MAIN: Построим массив mayArr...");
var myArr = buildArray(10);

info("MAIN: Распечатаем массив mayArr...");
printArray(myArr);

info("MAIN: Сортируем массив...");
sortArrayBubbles(myArr);

info("MAIN: Снова распечатаем массив mayArr...");
printArray(myArr);

info("MAIN: Теперь запустим тестирование!");

test("тестируем сложение (проверка тестирования)", 12 + 12, 24);
test("тестируем истину (проверка тестирования)", true, true);
//знаем, что в javascript-е функция void(0) - всегда возвращает undefined.
test("тестируем undefined (проверка тестирования)", void(0), undefined);
test("Тестируем сортировку массива методом пузырька", sortArrayBubbles([10, 1, 9, 2, 8, 3, -1, 4]), [-1, 1, 2, 3, 4, 8, 9, 10]);

info("MAIN: Программа завершена. До свидания! Колличество миллисекунд от 01.01.1970: " + new Date().getTime());







// [1:36:25 PM] Alex Kalinin (hedin): Твоя задача -- весь этот код переписать, осмыслить, добавить в него твои функции сотрировки
// [1:37:00 PM] Alex Kalinin (hedin): Нужно именно что переписать, руками, для того, чтобы увидеть и прочувствовать каждый символ
// [1:38:11 PM | Изменены 1:39:23 PM] Alex Kalinin (hedin): Тут я применил
// * Логгирование (функция info)
// * Регулярные выражения (функция colorizeString)
// * Юнит-тестирование (самописная функция test)

// Функцию colorizeString пока можешь не рабирать, а вот весь остальной код тебе НУЖЕН


// Также обращаю внимание на модульность:
// * если не нравится как делается разукраска текста --- поменяй код только внутри colorizeString.
// * Хочешь отключить подсветку - закомментируй строку, где строка пропускается через colorizeString
// * Если хочешь, чтобы информацию info выводил как-то по-другому, нужно поменять код только внутри info, ничего не сломается. Можно вообще все тело info закомментировать, и программа будет работать
// * toS() -- функция для конвертации объекта в строку. Не все объекты можно "склеить" в строку, т.е.
// var a = "" + {field: "value"} + " other string"
// получится

// [object] other string

// вместо

// field: value other string