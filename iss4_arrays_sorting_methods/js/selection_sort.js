//Sort an array of simple search

// Using the method sort() in javascript

var array = [26, 9, 64, 89, -65, 2, -8, 6, 0, 54, -68, 15, 1];
array.sort(function(a,b){return a-b});
console.log(array);

// Using variables , loops , functions

function selectionSort(array) {
    var array = [20, 19, 4, 89, -35, 2, -4, 7, 0, 44, -65, 16, 1];
    var n = array.length;
    // debugger;
    for (var j = 0; j < n-1; j++){
        var min = j;
        for (var i = j+1; i < n; i++){
            if (array[i] < array[min]){
                min = i;
            }
        }
        // debugger;
        if (min != j){
            var temp = array[j];
            array[j] = array[min];
            array[min] = temp;
        }
    }
    console.log(array);
    // return array;
}

