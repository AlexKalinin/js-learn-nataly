/**
 * ключ для хранилища куки и localStorage
 * @type {string}
 */
var STORAGE_KEY = 'iss2_storage';

/**
 * таймаут в днях для хранения значения в куках или localStorage
 * @type {number}
 */
var STORAGE_EXPIRATION_TIMEOUT = 1;

/**
 * проверяет переданный параметр num, является ли он целочисленным числом
 *
 * @param num - проверяемое значение
 * @returns {boolean} true если целое число, false в остальных случаях
 */
function isInteger(num) {
    return (num ^ 0) === num;
}

/**
 * Cгенерирует таблицу с разядностью rows X columns (rows-строк на columns-столбцов)
 * 
 * Например, если вызвать эту функцию так:
 *   generateTableObject(2, 4);
 * 
 *   то сформируется объект, представляющий собой таблицу:
 * 
 *   ---------------------------------
 *   |  xxx  |  xxx  |  xxx  |  xxx  |
 *   ---------------------------------
 *   |  xxx  |  xxx  |  xxx  |  xxx  |
 *   ---------------------------------
 */
function generateTableObject(rows, columns) {
    if (!isInteger(rows) || rows < 1) {
        return [];
    }
    if (!isInteger(columns) || columns < 1) {
        return [];
    }
    var bigTable = [];
    for (var i = 0; i < rows; i++) {
        var row = [];
        for (var j = 0; j < columns; j++) {
            row.push(Math.random());
        }
        bigTable.push(row);
    }
    return bigTable;
}

/**
 * Сгенерировать случайное целое число в диапазоне от min до max
 * @param min - нижняя граница
 * @param max - верхняя граница
 * @returns {integer} возвращает случайное целое число
 */
function getRandomInt(min, max){
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * распечатывает таблицу в консоль
 * @see generateTableObject()
 * @param obj
 */
function printTableObject(obj){
  for(var i = 0; i < obj.length; i++){
    for(var j = 0; j < obj[i].length; j++){
     console.log("Строка", i, "Столбец", j, "Значение", obj[i][j]);
    }
  }
}
/**
 * установка cookies
 * @param name
 * @param value
 * @param expires
 */
function set_cookie(name, value, expires)
{
    if (!expires)
    {
        expires = new Date();
    }
    document.cookie = name + "=" + JSON.stringify(value) + "; expires=" + expires.toGMTString() +  "; path=/";
}

function saveToCookies(obj){
    var expires = new Date();
    expires.setTime(expires.getTime() + (1000 * 60 * 60 * 24 * STORAGE_EXPIRATION_TIMEOUT));
    set_cookie(STORAGE_KEY, obj, expires);
}

function loadFromCookies(){
    var cookie_name = STORAGE_KEY + "=";
    var cookie_length = document.cookie.length;
    var cookie_begin = 0;
    while (cookie_begin < cookie_length)
    {
        var value_begin = cookie_begin + cookie_name.length;
        if (document.cookie.substring(cookie_begin, value_begin) === cookie_name)
        {
            var value_end = document.cookie.indexOf (";", value_begin);
            if (value_end === -1)
            {
                value_end = cookie_length;
            }
            return JSON.parse(document.cookie.substring(value_begin, value_end));
        }
        cookie_begin = document.cookie.indexOf(" ", cookie_begin) + 1;
        if (cookie_begin === 0)
        {
            break;
        }
    }
    return null;
}

/**
 * сохранить obj в localStorage
 * @param obj
 */
function saveToLocalStorage(obj){
    localStorage.setItem(STORAGE_KEY, JSON.stringify(obj));
}

/**
 * получить объект из localStorage
 * @returns {*}
 */
function loadFromLocalStorage(){
    return JSON.parse(localStorage.getItem(STORAGE_KEY));
}



/**
 * точка входа в программу
 */
function run(){
    //теперь при каждой перезагрузке страницы будет формироваться случайная таблица
    //поскольку это уже не жестко приявязанная 2x4 таблица, то буду называть ее не tbl2x4, а   myTable
    var rows = getRandomInt(1, 10);
    var cols = getRandomInt(1, 10);
    console.log('Сейчас распечатаем таблицу с ' + rows + ' рядами и ' + cols + ' столбцами:');
    var myTable = generateTableObject(rows, cols);
    printTableObject(myTable);


}


function run04(){
    var tbl3x4 = generateTableObject(3, 4);

    saveToCookies(tbl3x4);
    var tbl3x4_restored = loadFromCookies();

    printTableObject(tbl3x4);
    console.log('-----------------------');
    printTableObject(tbl3x4_restored);
}

function run0504(){
    var tbl3x4 = generateTableObject(3, 4);

    saveToLocalStorage(tbl3x4);
    var tbl3x4_restored = loadFromLocalStorage();

    printTableObject(tbl3x4);
    console.log('-----------------------');
    printTableObject(tbl3x4_restored);
}
