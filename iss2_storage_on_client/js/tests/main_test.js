describe('testing iss2_storage_on_client (main.js)',function(){
    it('isInteger(): should determine is number correctly', function(){
        expect(isInteger(-5)).toEqual(true);
        expect(isInteger(5)).toEqual(true);
        expect(isInteger(0)).toEqual(true);
        expect(isInteger('')).toEqual(false);
        expect(isInteger('5')).toEqual(false);
        expect(isInteger('aaa5')).toEqual(false);
        expect(isInteger('5aaa')).toEqual(false);
        expect(isInteger('aaa')).toEqual(false);
        expect(isInteger(5.5)).toEqual(false);
        expect(isInteger([])).toEqual(false);
        expect(isInteger([1])).toEqual(false);
        expect(isInteger({})).toEqual(false);
        expect(isInteger({number:5})).toEqual(false);
        expect(isInteger(false)).toEqual(false);
        expect(isInteger(undefined)).toEqual(false);

    });


    it('generateTableObject(): should generateTableObject with correct values', function(){
        var tbl = generateTableObject(5, 10);
        //debugger;
        expect(tbl.length).toEqual(5);
        for(var i = 0; i < tbl.length; i++){
            expect(tbl[i].length).toEqual(10);
        }

    });

    it('generateTableObject(): should generate empty table, when row or column is zero or unsigned', function(){
        var tbl_1 = generateTableObject(0, 10);
        expect(tbl_1).toEqual([]);
        expect(tbl_1.length).toEqual(0);

        var tbl_2 = generateTableObject(10, 0);
        expect(tbl_2).toEqual([]);
        expect(tbl_2.length).toEqual(0);

        var tbl_3 = generateTableObject(-4, 10);
        expect(tbl_3).toEqual([]);
        expect(tbl_3.length).toEqual(0);

        var tbl_4 = generateTableObject(15, -10);
        expect(tbl_4).toEqual([]);
        expect(tbl_4.length).toEqual(0);
    });

    var objects = [
        //тестируем строки
        '',
        'english string',
        'русская строка',
        'spec chars !@#$%^&*()_+=-][}{|`~',

        //тестируем числа
        1,
        -1,
        0.35466,

        //примитивы
        true,
        false,
        null,
        //NaN,
        //undefined,

        //тестируем массивы
        [],
        ['apple', 'cherry'],
        [1, 1, 2, 1, 3, 8],
        [
            [1, 5, 2, 1, 3, 8],
            [1, 1, 5, 1],
            [1, 3, 8],
            [1, 3, 2, 1, 6]
        ],
        generateTableObject(5, 3),

        //объекты
        {},
        {
            age: 15,
            name: 'Вася'
        }

    ];


    it('saveToCookies() / loadFromCookies(): should not change object', function(){
        for(var m = 0; m < objects.length; m++){
            saveToCookies(objects[m]);
            var obj = loadFromCookies();
            expect(objects[m]).toEqual(obj);
        }
    });

    it('saveToLocalStorage() / loadFromLocalStorage: should not change object', function(){
        for(var a = 0; a < objects.length; a++){
            saveToLocalStorage(objects[a]);
            var obj = loadFromLocalStorage();
            expect(objects[a]).toEqual(obj);
        }
    });
});