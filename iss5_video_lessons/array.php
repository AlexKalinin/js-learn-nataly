<?php

echo '<p><strong>Массивы</strong></p>';
//Массивы

$student = array('Иванов', 'Петров', 'Сидоров');//нумерованный массив
$student[2] = 'Лим';
print_r($student);
echo $student[1] . "<br />";

$fruits = ['апельсин', 'банан', 'яблоко'];
print_r($fruits);
echo $fruits[0] . "<br />";

$sin = ['стол' => 'дубовый', 'стул' => 'железный'];//ассоциативный массив
print_r($sin);
echo $sin['стол'] . "<br />";

echo '<p><strong>Циклы</strong></p>';
//Циклы

foreach ($student as $key => $value) {
    echo $value . ' студент<br>';
}

for ($i = 0; $i < 5; $i++) {
    echo $i . '<br>';
}

echo '<p><strong>Ссылки</strong></p>';
//Ссылки

$a = 5;
$b = &$a;

$b = 17;
$a = 2;
echo $a . '<br>';