<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>online shop</title>
    <link rel="stylesheet" href="views/base.css">
    <link rel="stylesheet" href="views/style.css">
</head>
<body>
<div id="wrapper">
    <div class="main-menu-wrapper">
        <div class="main-menu">
            <ul class="homepage">
                <li><a href="?page=include">Mini market</a></li>
            </ul>
            <ul class="menu-right">
                <li><a href="?page=about">О Компании</a></li>
                <li class="cart"><a href="?page=cart">Корзина</a></li>
            </ul>
        </div>
    </div>
    <div style="clear: both"></div>

    <div id="content">
        <div class="main">
