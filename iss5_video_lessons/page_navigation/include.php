<?php

require 'views/header.php';


//echo 'Тело приложения';
//var_dump($_GET);

switch($_GET['page']){
    case 'include':
        include 'pages/main.php';
        break;
    case 'cart':
        include 'pages/cart.php';
        break;
    case 'about':
        include 'pages/about.php';
        break;
    default:
        include 'pages/error.php';
        break;
}

require 'views/footer.php';