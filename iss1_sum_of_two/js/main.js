//получить значение элемента с id="firstNumber"
function getFirstNumber(){
    var elem = document.getElementById("firstNumber");
    return parseFloat(elem.value);
}

//получить значение элемента с id="secondNumber"
function getSecondNumber(){
    var elem = document.getElementById("secondNumber");
    return parseFloat(elem.value);
}

//установить значение val в textarea с id="result"
function setResult(val){
    var elem = document.getElementById("result");
    elem.value = val;
}

//функция вычисления сложения
function add(a, b){
    
    
    if(typeof(a) === 'number' && typeof(b) === 'number'){
       return a + b; 
    }
    
    return 'недопустимые значения!';
}

//Нажатие на кнопку Run
function buttonRun_onClick(){
    var a = getFirstNumber();
    var b = getSecondNumber();
    var res = add(a, b);
    setResult(res);
}


