// файл для тестирования js/main.js файла
// тут тестируется только логика изменения данных, 
// например функция add, которая должна сложить 2 значения и выдать результат

//описываем, что мы будем тестировать. Данная строка будет заголовком для теста.
describe('testing iss1_sum_of_two (main.js)', function() {
  
  //рассматриваем группу частных случаев
  //в данном случае -- тестирование сложения ЦЕЛЫХ чисел
  it('should add INTEGER numbers', function() {
    expect(add(1, 2)).toEqual(3);
    expect(add(-1, -2)).toEqual(-3);
    expect(add(-1, 2)).toEqual(1);
    expect(add(0,5)).toEqual(5);
    //ожидаем, что результат сложения чисел 10 и 20 будет равен 30.
    //Если по факту, функция add вернет, скажем не 30, а 31 -- мы увидем, что тест провален
    //таким образом, после каждого запуска автотестирования, мы всегда знаем работает ли функция add правильно
    expect(add(10, 20)).toEqual(30);
    expect(add(-10, 20)).toEqual(10);
  });


  //рассматриваем группу частных случаев
  //в данном случае -- тестирование сложения ДРОБНЫХ чисел
  it('should add FLOAT numbers', function() {
    expect(add(1.1, 2)).toEqual(3.1);
    expect(add(-1.1, -2)).toEqual(-3.1);
    expect(add(-10, 2.5)).toEqual(-7.5);
  });


  it('should handle non-number values', function(){
    var rez = add (1, 5);
    expect(rez).toEqual(6);
    // debugger;
    expect(add('foo', 1)).toEqual('недопустимые значения!');
    expect(add(555, 'foo')).toEqual('недопустимые значения!');

  });
  
});