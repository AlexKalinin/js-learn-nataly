# Наталья, изучение JavaScript
[![Code Climate](https://codeclimate.com/repos/57316f178c7b840080005f70/badges/022d90b0fd328bc095fa/gpa.svg)](https://codeclimate.com/repos/57316f178c7b840080005f70/feed)
Проект по изучению JavaScript.

# Оглавление

* [Тикет-пример](https://bitbucket.org/zencodepro/js-learn-nataly/issues/1/sum-of-two)
* [Wiki-страница, инструкции](https://bitbucket.org/zencodepro/js-learn-nataly/wiki/Home)
* [Исходный код задач](https://bitbucket.org/zencodepro/js-learn-nataly/src)
* [Коммиты](https://bitbucket.org/zencodepro/js-learn-nataly/commits/all)
* [Источники новых задач](https://bitbucket.org/zencodepro/js-learn-nataly/wiki/Home#markdown-header-)
